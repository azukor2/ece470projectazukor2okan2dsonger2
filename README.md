# Build 
You must also have [ign_moveit2](https://github.com/AndrejOrsula/ign_moveit2) in your workspace. Follow their instructions to set up that package.

To build run: 
```bash
colcon build --merge-install --symlink-install --cmake-args "-DCMAKE_BUILD_TYPE=Release"
```

To execute, run
```bash
ros2 launch ece470project customAction.launch.py
```

