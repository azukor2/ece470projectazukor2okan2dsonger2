#!/usr/bin/env python3

from geometry_msgs.msg import Pose
from moveit2 import MoveIt2Interface
from rclpy.node import Node
import rclpy
import time
from threading import Thread


class Thrower(Node):

    def __init__(self):
        super().__init__("thrower")

        self.brick_pos = [[0,0,0], [0,0,0], [0,0,0], [0,0,0]]

        self._object_pose_sub = self.create_subscription(Pose, '/model/brick1/pose', self.pose_callback_gen(0), 1)
        self._object_pose_sub = self.create_subscription(Pose, '/model/brick2/pose', self.pose_callback_gen(1), 1)
        self._object_pose_sub = self.create_subscription(Pose, '/model/brick3/pose', self.pose_callback_gen(2), 1)
        self._object_pose_sub = self.create_subscription(Pose, '/model/brick4/pose', self.pose_callback_gen(3), 1)

        # Create MoveIt2 interface node
        self._moveit2 = MoveIt2Interface()
        self._moveit2.set_max_velocity(0.5)
        self._moveit2.set_max_acceleration(0.5)

        # Create multi-threaded executor
        self._executor = rclpy.executors.MultiThreadedExecutor(2)
        self._executor.add_node(self)
        self._executor.add_node(self._moveit2)

        # Wait a couple of seconds until Ignition is ready and spin up the executor
        time.sleep(5)
        thread = Thread(target = self.build_house)
        self.get_logger().info("Starting Thread")
        thread.start()

        self._executor.spin()

    def pose_callback_gen(self, blockNum):
        def object_pose_callback(pose_msg):
            object_position = pose_msg.position
            self.brick_pos[blockNum] = [object_position.x, object_position.y, object_position.z]
        return object_pose_callback

    def build_house(self):
        self.get_logger().info("Thread Started")
        time.sleep(2)
        self.get_logger().info("Building the House")
        self.get_logger().info(str(self.brick_pos))

        
        block1Pos2 = [0.6, 0.3,0.025]
        self.pick_place(start=self.brick_pos[0], end=block1Pos2)

        block2Pos2 = [0.6522, 0.3,0.025]
        self.pick_place(start=self.brick_pos[1], end=block2Pos2)

        self.get_logger().info(str(self.brick_pos))

        block3Pos2 = self.brick_pos[0][:]
        block3Pos2[2]+=0.025
        self.pick_place(start=self.brick_pos[2], end=block3Pos2)

        block4Pos2 = self.brick_pos[1][:]
        block4Pos2[2]+=0.025
        self.pick_place(start=self.brick_pos[3], end=block4Pos2)

        self.go_home()


        self.get_logger().info(str(self.brick_pos))

        rclpy.shutdown()
        exit(0)

    def pick_place(self, start, end):
        quaternion = [1.0, 0.0, 0.0, 0.0]

        self._moveit2.gripper_open()
        self._moveit2.wait_until_executed()

        abovestart = start[:]
        abovestart[1]+=0.017
        abovestart[2]+=0.1
        self._moveit2.set_pose_goal(abovestart, quaternion)
        self._moveit2.plan_kinematic_path()
        self._moveit2.execute()
        self._moveit2.wait_until_executed()

        adjustedstart = start[:]
        adjustedstart[1]+=0.017
        adjustedstart[2]-=0.01
        self._moveit2.set_pose_goal(adjustedstart, quaternion)
        self._moveit2.plan_kinematic_path()
        self._moveit2.execute()
        self._moveit2.wait_until_executed()

        self._moveit2.gripper_close(width=0.024)
        self._moveit2.wait_until_executed()

        self.get_logger().info("Going Home")

        self.get_logger().info("Going to End")
        endYOffset = 0.0122
        aboveEnd = end[:]
        aboveEnd[2]+=0.1
        aboveEnd[1]+=endYOffset
        self._moveit2.set_pose_goal(aboveEnd, quaternion)
        self._moveit2.plan_kinematic_path()
        self._moveit2.execute()
        self._moveit2.wait_until_executed()

        adjustedend = end[:]
        adjustedend[1]+=endYOffset
        adjustedend[2]+=0.007
        self._moveit2.set_pose_goal(adjustedend, quaternion)
        self._moveit2.plan_kinematic_path()
        self._moveit2.execute()
        self._moveit2.wait_until_executed()

        self._moveit2.gripper_open()
        self._moveit2.wait_until_executed()

        self._moveit2.set_pose_goal(aboveEnd, quaternion)
        self._moveit2.plan_kinematic_path()
        self._moveit2.execute()
        self._moveit2.wait_until_executed()

    home_position = [0.6,0,0.4]
    def go_home(self):
        quaternion = [1.0, 0.0, 0.0, 0.0]
        self._moveit2.set_pose_goal(self.home_position, quaternion)
        self._moveit2.plan_kinematic_path()
        self._moveit2.execute()
        self._moveit2.wait_until_executed()



def main(args=None):
    rclpy.init(args=args)

    _thrower = Thrower()
    time.sleep(90)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
